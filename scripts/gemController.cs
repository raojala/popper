using Godot;

public class GemController : Node2D
{
    [Export] private GemType gemType = 0;
    [Export] private int reward = 0;
    private bool BeingDestroyed = false;

    public override void _Ready()
    {
        initShader();

        Gem_Events.GemDetonationEventHandler += checkGemDetonation_Event;
        Nugem_Events.NugemDetonationEventHandler += checkNugemDetonation_Event;
        CornerBomb_Events.CornerBombDetonationEventHandler += checkCornerBombDetonation_Event;
    }

    protected override void Dispose(bool disposing)
    {
        Gem_Events.GemDetonationEventHandler -= checkGemDetonation_Event;
        Nugem_Events.NugemDetonationEventHandler -= checkNugemDetonation_Event;
        CornerBomb_Events.CornerBombDetonationEventHandler -= checkCornerBombDetonation_Event;
    }

    private void checkGemDetonation_Event(object sender, Gem_Events.GemDetonationEventArgs e)
    {
        if (!BeingDestroyed)
        {
            GemDetonation(e.GemPosition, e.GemType);
        }
    }

    private void checkNugemDetonation_Event(object sender, Nugem_Events.NugemDetonationEventArgs e)
    {
        if (!BeingDestroyed)
        {
            NuGemDetonation(e.NugemPosition, e.GemType);
        }
    }

    private void checkCornerBombDetonation_Event(object sender, CornerBomb_Events.CornerBombDetonationEventArgs e)
    {
        if (!BeingDestroyed)
        {
            CornerBombDetonation(e.CornerBombPosition, e.GemType);
        }
    }

    private void GemDetonation(Vector2 clickPosition, GemType clickedGemType)
    {
        if (clickPosition.DistanceTo(Position) == Level.TILE_SIZE && (clickedGemType == gemType))
        {
            GemDestroy();
        }
    }

    private void NuGemDetonation(Vector2 clickPosition, GemType clickedGemType)
    {
        if (clickPosition.DistanceTo(Position) < Level.TILE_SIZE * NugemController.NUGEM_RANGE)
        {
            OtherDestroy();
        }
    }

    private void CornerBombDetonation(Vector2 clickPosition, GemType clickedGemType)
    {
        if (!BeingDestroyed && (clickPosition.x == Position.x || clickPosition.y == Position.y))
        {
            OtherDestroy();
        }
    }

    public override void _UnhandledInput(InputEvent inputEvent)
    {
        CheckMouseClick(inputEvent);
    }

    void CheckMouseClick(InputEvent inputEvent)
    {
        if (inputEvent.GetType() != typeof(InputEventMouseButton))
        {
            return;
        }

        InputEventMouseButton mouseButton = inputEvent as InputEventMouseButton;
        Sprite sprite = GetNode<Sprite>("Sprite");
        Rect2 SpriteRect = sprite.GetRect();
        if (mouseButton.IsPressed() && SpriteRect.HasPoint(ToLocal(mouseButton.Position)))
        {
            GemDestroy();
        }
    }

    void GemDestroy()
    {
        BeingDestroyed = true;
        Global.GameManager.AddScore(reward);
        Gem_Events.InvokeDetonateGem(this, gemType, Position);
        Gem_Events.InvokeSpawnGem(this, Position);
        QueueFree();
    }

    void OtherDestroy()
    {
        BeingDestroyed = true;
        Global.GameManager.AddScore(reward);
        Gem_Events.InvokeSpawnGem(this, Position);
        QueueFree();
    }

    private void initShader()
    {
        Sprite sprite = GetNode<Sprite>("Sprite");
        Material duplicate = sprite.Material.Duplicate() as Material;
        sprite.Material = duplicate;
        Material material = sprite.Material;
        ShaderMaterial shader = material as ShaderMaterial;
        shader.SetShaderParam("speed", GD.RandRange(0, 1));
    }
}