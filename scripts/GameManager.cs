using System.Threading;

public static partial class Global
{
    public static PlayerProfile Player { get; set; } = new PlayerProfile();
    public static GameManager GameManager { get; private set; } = new GameManager();

    public static void ResetGame()
    {
        GameManager = new GameManager();
        SceneManager_Events.InvokeSceneChanged("menu");
    }
}

public class GameManager
{
    public ulong Score { get; private set; }
    public int Lifes { get; private set; } = 5;

    public GameManager()
    {
        INIParser<PlayerProfile> parser = new INIParser<PlayerProfile>();
        parser.ParseData("player-profile.txt");
        Global.Player = parser.Value != null ? parser.Value : new PlayerProfile();
    }

    public void AddScore(int points)
    {
        combo++;
        if (combo == 1)
        {
            StartCombo();
        }
        tempScore += (ulong)points;
    }

    public void HitMine()
    {
        combo++;
        if (combo == 1)
        {
            StartCombo();
        }
        hitMine = true;
    }

    public void FlattenScore()
    {
        combo++;
        if (combo == 1)
        {
            StartCombo();
        }
        isFlatScore = true;
    }

    private void StartCombo()
    {
        Timer t = new Timer(FinishCombo, null, -1, Timeout.Infinite);
        t.Change(200, Timeout.Infinite);
    }

    private void FinishCombo(object state)
    {
        if (combo == 2)
        {
            Lifes -= 1;
        }
        else if (combo == 1)
        {
            Lifes -= 2;
        }

        if (isFlatScore)
        {
            combo = 1;
        }

        ulong comboScore = tempScore * combo;
        combo = 0;
        tempScore = 0;

        if (hitMine)
        {
            hitMine = false;
            Score -= comboScore;
            Lifes--;
        }
        else
        {
            Score += comboScore;
        }

        if (Lifes <= 0)
        {
            Highscores.SaveScore(Score, Global.Player.Name);
            Global.ResetGame();
        }
    }

    private ulong combo = 0;
    private ulong tempScore = 0;
    private bool hitMine = false;
    private bool isFlatScore = false;
}