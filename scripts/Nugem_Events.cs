using System;
using Godot;

class Nugem_Events
{
    public class NugemDetonationEventArgs : EventArgs
    {
        public GemType GemType { get; set; }
        public Vector2 NugemPosition { get; set; }
    }
    public delegate void NugemDetonationEvent(object sender, NugemDetonationEventArgs e);
    public static event NugemDetonationEvent NugemDetonationEventHandler;
    public static void InvokeDetonateNugem(object sender, GemType gemType, Vector2 nugemPosition)
    {
        var ev = new NugemDetonationEventArgs();
        ev.GemType = gemType;
        ev.NugemPosition = nugemPosition;
        NugemDetonationEventHandler.Invoke(sender, ev);
    }
}