using Godot;
using System;

public class lblScore : Label
{
    string scoreDefaultText = "Score: ";

    public override void _Ready()
    {
        AlignToLeft();
    }

    private void AlignToLeft()
    {
        int MarginLeft = 10;
        int MarginTop = -2;
        RectPosition = new Vector2(MarginLeft, MarginTop);
    }

    public override void _Process(float delta)
    {
        Text = scoreDefaultText + Global.GameManager.Score;
    }
}
