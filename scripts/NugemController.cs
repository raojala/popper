using Godot;

public class NugemController : Node2D
{
    public static int NUGEM_RANGE = 4;

    private GemType gemType = GemType.Nugem;
    private bool BeingDestroyed = false;

    public override void _Ready()
    {
        Gem_Events.GemDetonationEventHandler += checkGemDetonation_Event;
        Nugem_Events.NugemDetonationEventHandler += checkNugemDetonation_Event;
        CornerBomb_Events.CornerBombDetonationEventHandler += checkCornerBombDetonation_Event;
        initShader();
    }

    protected override void Dispose(bool disposing)
    {
        Gem_Events.GemDetonationEventHandler -= checkGemDetonation_Event;
        Nugem_Events.NugemDetonationEventHandler -= checkNugemDetonation_Event;
        CornerBomb_Events.CornerBombDetonationEventHandler -= checkCornerBombDetonation_Event;
    }

    private void checkGemDetonation_Event(object sender, Gem_Events.GemDetonationEventArgs e)
    {
        GemDetonation(e.GemPosition, e.GemType);
    }

    private void checkNugemDetonation_Event(object sender, Nugem_Events.NugemDetonationEventArgs e)
    {
        NuGemDetonation(e.NugemPosition, e.GemType);
    }

    private void checkCornerBombDetonation_Event(object sender, CornerBomb_Events.CornerBombDetonationEventArgs e)
    {
        CornerBombDetonation(e.CornerBombPosition, e.GemType);
    }

     private void GemDetonation(Vector2 clickPosition, GemType clickedGemType)
    {
        if (!BeingDestroyed && clickPosition.DistanceTo(Position) == Level.TILE_SIZE)
        {
            NugemDestroy();
        }
    }

    private void NuGemDetonation(Vector2 clickPosition, GemType clickedGemType)
    {
        if (!BeingDestroyed && clickPosition.DistanceTo(Position) < Level.TILE_SIZE * NUGEM_RANGE)
        {
            NugemDestroy();
        }
    }

    private void CornerBombDetonation(Vector2 clickPosition, GemType clickedGemType)
    {
        if (!BeingDestroyed && (clickPosition.x == Position.x || clickPosition.y == Position.y))
        {
            NugemDestroy();
        }
    }

    public override void _UnhandledInput(InputEvent inputEvent)
    {
        CheckMouseClick(inputEvent);
    }

    void CheckMouseClick(InputEvent inputEvent)
    {
        if (inputEvent.GetType() != typeof(InputEventMouseButton))
        {
            return;
        }

        InputEventMouseButton mouseButton = inputEvent as InputEventMouseButton;
        Sprite sprite = GetNode<Sprite>("Sprite");
        Rect2 SpriteRect = sprite.GetRect();
        if (mouseButton.IsPressed() && SpriteRect.HasPoint(ToLocal(mouseButton.Position)))
        {
            NugemDestroy();
        }
    }

    void NugemDestroy()
    {
        BeingDestroyed = true;
        Global.GameManager.FlattenScore();
        Nugem_Events.InvokeDetonateNugem(this, gemType, Position);
        Gem_Events.InvokeSpawnGem(this, Position);
        QueueFree();
    }

    private void initShader()
    {
        Sprite sprite = GetNode<Sprite>("Sprite");
        Material duplicate = sprite.Material.Duplicate() as Material;
        sprite.Material = duplicate;
        Material material = sprite.Material;
        ShaderMaterial shader = material as ShaderMaterial;
        shader.SetShaderParam("speed", GD.RandRange(0, 1));
    }
}