using System.Collections.Generic;
using System.IO;

public static class Highscores
{
    public static CsvParser<Highscore> parser = new CsvParser<Highscore>("highscores.csv", ';', ",");
    public static List<Highscore> ListScores()
    {
        return parser.Parse();
    }

    public static void SaveScore(ulong score, string name)
    {
        Highscore hScore = new Highscore();
        hScore.Score = score;
        hScore.Name = name;
        parser.CreateFileWithHeaders();
        parser.CreateLine(hScore);
    }
}

public class Highscore
{
    public string Name { get; set; }
    public ulong Score { get; set; }
}