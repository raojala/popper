using Godot;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

public class Level : Node2D
{
    public static int MENU_HEIGHT = 35;
    public static int TILE_SIZE = 32;
    Vector2 Screen;

    private WeightedTable<PackedScene> gems = new WeightedTable<PackedScene>();
    private List<List<Vector2>> Grid = new List<List<Vector2>>();

    List<Task> spawnTimers = new List<Task>();

    CancellationTokenSource cancellationContext;
    CancellationToken context;

    public override void _Ready()
    {
        cancellationContext = new CancellationTokenSource();
        context = cancellationContext.Token;

        Screen = GetViewport().Size;

        CenterPlayField();
        SetupWeightedTable();
        SetupGrid();
        InitBoard();

        Gem_Events.GemSpawningEventHandler += GemSpawning_Event;
    }

    protected override void Dispose(bool disposing)
    {
        Gem_Events.GemSpawningEventHandler -= GemSpawning_Event;
        cancellationContext.Cancel();
    }

    void GemSpawning_Event(object sender, Gem_Events.GemSpawningEventArgs e)
    {
        DelayedGemSpawn(e.GemPosition);
    }

    private void CenterPlayField()
    {
        int gridXcount = (int)Screen.x / TILE_SIZE;
        int XOffset = (int)Screen.x - TILE_SIZE * gridXcount;

        int gridYcount = (int)(Screen.y - MENU_HEIGHT) / TILE_SIZE;
        int YOffset = (int)Screen.y - MENU_HEIGHT - TILE_SIZE * gridYcount;

        Position = new Vector2(Position.x + XOffset / 2, Position.y + MENU_HEIGHT + YOffset / 2);
    }

    private void SetupWeightedTable()
    {
        PackedScene redGem = ResourceLoader.Load<PackedScene>("res://Prefabs/red-gem.tscn");
        PackedScene greenGem = ResourceLoader.Load<PackedScene>("res://Prefabs/green-gem.tscn");
        PackedScene blueGem = ResourceLoader.Load<PackedScene>("res://Prefabs/blue-gem.tscn");
        PackedScene tealRuby = ResourceLoader.Load<PackedScene>("res://Prefabs/teal-ruby.tscn");
        PackedScene zergRuby = ResourceLoader.Load<PackedScene>("res://Prefabs/zerg-ruby.tscn");
        PackedScene yellowRuby = ResourceLoader.Load<PackedScene>("res://Prefabs/yellow-ruby.tscn");

        gems.Add(redGem, 60);
        gems.Add(greenGem, 60);
        gems.Add(blueGem, 60);
        gems.Add(tealRuby, 30);
        gems.Add(zergRuby, 30);
        gems.Add(yellowRuby, 30);
    }

    private void AddSpecialGemsToPool()
    {
        PackedScene nugem = ResourceLoader.Load<PackedScene>("res://Prefabs/nugem.tscn");
        gems.Add(nugem, 3);

        PackedScene cornerBomb = ResourceLoader.Load<PackedScene>("res://Prefabs/corner-bomb.tscn");
        gems.Add(cornerBomb, 5);

        PackedScene mine = ResourceLoader.Load<PackedScene>("res://Prefabs/mine.tscn");
        gems.Add(mine, 10);
    }

    private void SetupGrid()
    {
        int gridXcount = (int)Screen.x / TILE_SIZE;
        int gridYcount = (int)(Screen.y - MENU_HEIGHT) / TILE_SIZE;

        for (int x = 0; x < gridXcount; x++)
        {
            Grid.Add(new List<Vector2>());
            for (int y = 0; y < gridYcount; y++)
            {
                Grid[x].Add(new Vector2(TILE_SIZE * x + (TILE_SIZE / 2), (TILE_SIZE) * y + (TILE_SIZE / 2)));
            }
        }
    }

    private void InitBoard()
    {
        foreach (var row in Grid)
        {
            foreach (var position in row)
            {
                NewGem(position);
            }
        }

        AddSpecialGemsToPool();
    }

    async void DelayedGemSpawn(Vector2 position)
    {
        try
        {
            Task t = Task.Delay(1000, context);
            spawnTimers.Add(t);
            await t;
            if (!t.IsCanceled)
                NewGem(position);
        }
        catch {} // don't care about this error
    }

    private void NewGem(object state)
    {
        Vector2 position = (Vector2)state;
        var Screen = GetViewport().Size;
        Node2D instance = Utils.Instantiate2D(this, gems.GetRandom());
        instance.Position = position;
    }
}