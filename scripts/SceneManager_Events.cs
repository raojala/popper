using System;
using Godot;

class SceneManager_Events
{
    public class SceneChangedEventArgs : EventArgs
    {
        public string SceneName { get; set; }
    }
    public delegate void SceneChangedEvent(SceneChangedEventArgs e);
    public static event SceneChangedEvent SceneChangedEventHandler;
    public static void InvokeSceneChanged(string sceneName)
    {
        var ev = new SceneChangedEventArgs();
        ev.SceneName = sceneName;
        SceneChangedEventHandler.Invoke(ev);
    }
}