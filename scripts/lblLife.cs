using Godot;
using System;

public class lblLife : Label
{
    string lifeDefaultText = "Lives: ";

    public override void _Ready()
    {
        AlignToRight();
    }

    private void AlignToRight()
    {
        int MarginRight = 10;
        int MarginTop = -2;
        Vector2 Screen;

        Screen = GetViewport().Size;
        var xPosition = Screen.x - RectSize.x - MarginRight;
        RectPosition = new Vector2(xPosition, MarginTop);
    }

    public override void _Process(float delta)
    {
        if (Global.GameManager.Lifes > 6)
        {
            Text = Global.GameManager.Lifes.ToString();
        }
        else
        {
            string text = "";
            for (int i = 0; i < Global.GameManager.Lifes; i++)
            {
                text += "I";
            }
            Text = text;
        }
    }
}
