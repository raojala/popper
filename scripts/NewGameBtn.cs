using Godot;

public class NewGameBtn : Button
{
    LineEdit nameTextBox = new LineEdit();
    public override void _Ready()
    {
        nameTextBox = GetParent().GetNode<LineEdit>("txtName");
        nameTextBox.Text = Global.Player.Name;
    }

    public override void _Pressed()
    {
        Global.Player.Name = nameTextBox.Text;
        if (Global.Player.Name.Length > 0)
        {
            try
            {
                INIParser<PlayerProfile> parser = new INIParser<PlayerProfile>();
                parser.WriteData(Global.Player, "player-profile.txt");
            }
            catch (System.Exception e)
            {
                GD.Print(e);
            }

            SceneManager_Events.InvokeSceneChanged("main");
        }
    }
}
