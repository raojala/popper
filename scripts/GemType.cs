public enum GemType
{
    Red,
    Green,
    Blue,
    Teal,
    Zerg,
    Yellow,
    Nugem,
    CornerBomb,
    Mine,
}