using System;
using Godot;

class CornerBomb_Events
{
    public class CornerBombDetonationEventArgs : EventArgs
    {
        public GemType GemType { get; set; }
        public Vector2 CornerBombPosition { get; set; }
    }
    public delegate void CornerBombDetonationEvent(object sender, CornerBombDetonationEventArgs e);
    public static event CornerBombDetonationEvent CornerBombDetonationEventHandler;
    public static void InvokeDetonateCornerBomb(object sender, GemType gemType, Vector2 cornerBombPosition)
    {
        var ev = new CornerBombDetonationEventArgs();
        ev.GemType = gemType;
        ev.CornerBombPosition = cornerBombPosition;
        CornerBombDetonationEventHandler.Invoke(sender, ev);
    }
}