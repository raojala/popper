using Godot;

class SceneManager : Node2D
{

    Node2D currentScene;

    public override void _Ready()
    {
        PackedScene menuPrefab = ResourceLoader.Load<PackedScene>("res://Scenes/menu.tscn");
        currentScene = Utils.Instantiate2D(this, menuPrefab);

        RegisterEvents();
    }

    protected override void Dispose(bool disposing)
    {
        DeRegisterEvents();
    }

    private void RegisterEvents()
    {
        SceneManager_Events.SceneChangedEventHandler += SceneChanged;
    }

    private void DeRegisterEvents()
    {
        SceneManager_Events.SceneChangedEventHandler -= SceneChanged;
    }

    public void SceneChanged(SceneManager_Events.SceneChangedEventArgs ev)
    {
        currentScene.QueueFree();
        PackedScene scenePrefab = ResourceLoader.Load<PackedScene>("res://Scenes/" + ev.SceneName + ".tscn");
        currentScene = Utils.Instantiate2D(this, scenePrefab);
    }
}