using System;
using Godot;

class Gem_Events
{
    public class GemDetonationEventArgs : EventArgs
    {
        public GemType GemType { get; set; }
        public Vector2 GemPosition { get; set; }
    }
    public delegate void GemDetonationEvent(object sender, GemDetonationEventArgs e);
    public static event GemDetonationEvent GemDetonationEventHandler;
    public static void InvokeDetonateGem(object sender, GemType gemType, Vector2 gemPosition)
    {
        var ev = new GemDetonationEventArgs();
        ev.GemType = gemType;
        ev.GemPosition = gemPosition;
        GemDetonationEventHandler.Invoke(sender, ev);
    }


    public class GemSpawningEventArgs : EventArgs
    {
        public Vector2 GemPosition { get; set; }
    }
    public delegate void GemSpawningEvent(object sender, GemSpawningEventArgs e);
    public static event GemSpawningEvent GemSpawningEventHandler;
    public static void InvokeSpawnGem(object sender, Vector2 gemPosition)
    {
        var ev = new GemSpawningEventArgs();
        ev.GemPosition = gemPosition;       
        GemSpawningEventHandler.Invoke(sender, ev);
    }
}