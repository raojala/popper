using System.Reflection;
using System.IO;
using System;

public class INIParser<T> where T : new()
    {
        public T Value = default(T);

        public void ParseData(string filePath)
        {
            try
            {
                /*
                 DataParser<YourClass> parser = new DataParser<YourClass>();
                 YourClass data = parser.ParseData("path/to/file.txt");
                */
                T data = new T();
                if (!File.Exists(filePath))
                {
                    return;
                }

                string[] lines = File.ReadAllLines(filePath);
                foreach (string line in lines)
                {
                    string[] parts = line.Split('=');
                    string key = parts[0];
                    string value = parts[1];

                    PropertyInfo property = typeof(T).GetProperty(key);
                    if (property != null)
                    {
                        Type propertyType = property.PropertyType;
                        object convertedValue = Convert.ChangeType(value, propertyType);
                        property.SetValue(data, convertedValue);
                    }
                }
                Value = data;
            }
            catch { throw; }
        }

        public void WriteData(T data, string filePath)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(filePath))
                {
                    PropertyInfo[] properties = typeof(T).GetProperties();
                    foreach (PropertyInfo property in properties)
                    {
                        string key = property.Name;
                        string value = property.GetValue(data).ToString();
                        writer.WriteLine("{0}={1}", key, value);
                    }
                }
            }
            catch { throw; }
        }
    }
