using System;
using System.Collections.Generic;

public class WeightedTable<T>
{
    private List<(int weight, T item)> items;
    private int[] prefixSums;
    internal readonly Random Random;

    public WeightedTable()
    {
        items = new List<(int weight, T item)>();
        prefixSums = new int[0];
        Random = new Random();
    }

    public void Add(T item, int weight)
    {
        items.Add((weight, item));
        BuildPrefixSums();
    }

    public T GetRandom()
    {
        if (prefixSums.Length < 1) 
        {
            return default(T);
        }
        
        int randomWeight = Random.Next(prefixSums[prefixSums.Length - 1]);
        int index = Array.BinarySearch(prefixSums, randomWeight);
        if (index < 0)
        {
            index = ~index;
        }
        return items[index].item;
    }

    private void BuildPrefixSums()
    {
        prefixSums = new int[items.Count];
        int sum = 0;
        for (int i = 0; i < items.Count; i++)
        {
            sum += items[i].weight;
            prefixSums[i] = sum;
        }
    }
}
