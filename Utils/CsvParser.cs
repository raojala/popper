using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System;
using System.IO;

public class CsvParser<T>
{
    private readonly string _filePath;
    private readonly Dictionary<string, int> _titles;
    private CultureInfo _cultureInfo;
    private readonly char _delimitter;

    private readonly Dictionary<string, Func<string, object>> _conversionFunctions;

    public CsvParser(string filePath, char delimitter = ',', string decimalSeparator = ".")
    {
        _filePath = filePath;
        _titles = new Dictionary<string, int>();
        _cultureInfo = new CultureInfo(CultureInfo.CurrentCulture.Name);
        _cultureInfo.NumberFormat.NumberDecimalSeparator = decimalSeparator;
        _delimitter = delimitter;
        _conversionFunctions = new Dictionary<string, Func<string, object>>();
    }

    public void SetCulture(CultureInfo culture)
    {
        _cultureInfo = culture;
    }

    public void AddConversionFunction(string propertyName, Func<string, object> conversionFunction)
    {
        _conversionFunctions[propertyName] = conversionFunction;
    }

    public List<T> Parse()
    {
        try
        {
            using (StreamReader reader = new StreamReader(_filePath))
            {
                ParseTitles(reader.ReadLine());

                List<T> result = CreateList();
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    T instance = CreateInstance();
                    MapValuesToProperties(instance, line);
                    result.Add(instance);
                }
                return result;
            }

        }
        catch
        {
            throw;
        }
    }

    private void ParseTitles(string line)
    {
        string[] titleValues = line.Split(_delimitter);

        for (int i = 0; i < titleValues.Length; i++)
        {
            _titles[titleValues[i]] = i;
        }
    }

    private List<T> CreateList()
    {
        return (List<T>)Activator.CreateInstance(typeof(List<>).MakeGenericType(typeof(T)));
    }

    private T CreateInstance()
    {
        return (T)Activator.CreateInstance(typeof(T));
    }

    private void MapValuesToProperties(T instance, string line)
    {
        string[] values = line.Split(_delimitter);

        foreach (KeyValuePair<string, int> title in _titles)
        {
            PropertyInfo property = typeof(T).GetProperty(title.Key);

            // Check if a conversion function has been provided for this property.
            // If not, use the default conversion function (Convert.ChangeType).
            Func<string, object> conversionFunction = _conversionFunctions.ContainsKey(title.Key)
                ? _conversionFunctions[title.Key]
                : (x => Convert.ChangeType(x, property.PropertyType, _cultureInfo));

            // Check if the value is surrounded by quotes. If it is, remove the quotes.
            string value = values[title.Value];
            if (value.Length > 1 && value[0] == '"' && value[value.Length - 1] == '"')
            {
                value = value.Substring(1, value.Length - 2);
            }

            // Convert the value using the specified conversion function and set it on the property.
            property.SetValue(instance, conversionFunction(value));
        }
    }

    public void CreateFileWithHeaders()
    {
        try
        {
            if (!File.Exists(_filePath))
            {
                using (StreamWriter writer = new StreamWriter(_filePath))
                {
                    PropertyInfo[] properties = typeof(T).GetProperties();
                    string headerLine = "";
                    for (int i = 0; i < properties.Length; i++)
                    {
                        headerLine += properties[i].Name;
                        if (i != properties.Length - 1)
                        {
                            headerLine += _delimitter;
                        }
                    }
                    writer.WriteLine(headerLine);
                }
            }
        }
        catch { throw; }
    }

    public void CreateLine(T instance)
    {
        PropertyInfo[] properties = typeof(T).GetProperties();
        string line = string.Join(_delimitter.ToString(), Array.ConvertAll(properties, property => property.GetValue(instance).ToString()));

        try
        {
            using (StreamWriter writer = new StreamWriter(_filePath, true))
            {
                writer.WriteLine(line);
            }
        }
        catch { throw; }
    }

}