using Godot;
using System;

public static class Utils
{
    public static Node2D Instantiate2D(Node2D parent, PackedScene obj) 
    {
        Node2D instance = (Node2D)obj.Instance();
        parent.AddChild(instance);
        return instance;
    }
}